<?php
namespace Departamento;
use Zend\Router\Http\Segment;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return array(
    'controllers' => array(
        'factories' => array(
           //Controller\CadSecaoController::class  => InvokableFactory::class,
            //Controller\CadGrupoController::class => InvokableFactory::class,
            //Controller\CadSubGrupoController::class => InvokableFactory::class,
        ),
        
        
    ),
    
    'router' => [
        'routes' => [
            'secao' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => "/secao[/:id]",
                    'constraints' => [
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller'    => Controller\SecaoController::class,
                        'order' => 'ASC',
                        'page' => 1,
                        'search_frase' => '',
                    ],
                ],
            ],
            
            'nextsecao' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => "/nextsecao[[/:action][/:id]]",
                    'constraints' => [
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller'    => Controller\SecaoController::class,
                        'order' => 'ASC',
                        'page' => 1,
                        'search_frase' => '',
                    ],
                ],
            ],
            
            'prevsecao' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => "/prevsecao[[/:action][/:id]]",
                    'constraints' => [
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller'    => Controller\SecaoController::class,
                        'order' => 'ASC',
                        'page' => 1,
                        'search_frase' => '',
                    ],
                ],
            ],

        ],
    ],
    
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
    
    'view_manager' => array(

        'template_path_stack' => array(
            'caddepartamento' => __DIR__ . '/../view',
            'cadsecao' => __DIR__ . '/../view',
            'cadgrupo' => __DIR__ . '/../view',
            'cadsubgrupo' => __DIR__ . '/../view',
        ),
        'strategies' => array(
    		'ViewJsonStrategy',
        ),
    ),
   
);