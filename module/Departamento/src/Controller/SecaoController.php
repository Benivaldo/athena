<?php

namespace Departamento\Controller;

use Controle\Controller\AbstractRestController;
use Zend\View\Model\JsonModel;
use Departamento\Entity\Secao;


class SecaoController extends AbstractRestController
{
    private function getVariaveis()
    {
        $this->route = 'secao';
        $this->viewData = 'dados';
        $this->pagination = true;
        $this->template = '';
        $this->div = '';
        $this->primaryKey = null;
        $this->order_by = 'id';
        $this->entity = '\Departamento\Entity\Secao';
        $this->model = new Secao();
        $this->searchFrase = '';
        $this->searchDate = '';
        $this->foreignKeys = array();
    }
    
    
    public function getList()
    {
        $this->getVariaveis();       
        
        return parent::getList();
    }
    
    
    public function get($id)
    {
        $this->getVariaveis();
        
        
        return parent::get($id);
    }
    
    public function create($data)
    {
        $this->getVariaveis();
        
        $this->getForeignKeys($data);
        
        return parent::create($data);
        
    }
    
    public function update($id, $data)
    {
        $this->getVariaveis();
        
        $this->getForeignKeys($data);
        
        return  parent::update($id, $data);
    }
    
    public function delete($id)
    {
        $this->getVariaveis();
        
        return parent::delete($id);
    }
    
    public function prevAction()
    {
        $this->getVariaveis();
        
        return parent::prevAction();
    }
    
    public function nextAction()
    {
        $this->getVariaveis();
        
        return parent::nextAction();
    }
    
    private function getForeignKeys($data)
    {
       
    }
}
