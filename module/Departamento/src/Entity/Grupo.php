<?php
namespace Departamento\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
//use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Departamento\Repository\GrupoRepository")
 * @ORM\Table(name="cad_grupo")
 */
class Grupo
{
	protected $inputFilter;
	
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(name="id")
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="descricao")
	 */
	protected $descricao;
	
	
	
	protected $secao_id;
	

	
	/**
	 * @var datetime
	 * @ORM\Column(name="date_create")
	 */
	protected $dataCadastro;
	
	/**
	 * @var datetime
	 * @ORM\Column(name="date_update")
	 */	
	protected $dataAltera;
	
	
	
	/**
	 * Returns ID of this secao
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Sets ID of this chamados.
	 * @param integer $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	
	/**
	 * Returns titulo.
	 */
	public function getDescricao()
	{
		return $this->descricao;
	}
	
/**
	 * Sets titulo.
	 * @param string $descricao
	 * @return \Departamento\Entity\Secao
	 */
	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;	
	}
	
	
	/**
	 * Gets triggered only on insert
	 * @ORM\PrePersist
	 */
	public function onPrePersist()
	{
		$this->dataCadastro = date('Y-m-d');
		$this->dataAltera = date('Y-m-d');
	}
	
	/**
	 * Gets triggered every time on update
	 * @ORM\PreUpdate
	 */
	public function onPreUpdate()
	{
		$this->dataAltera = date('Y-m-d');
	}
	
	public function exchangeArray($data)
	{
	    foreach ($data as $key => $value) {
	        $this->$key = (!empty($value) ? $value: null);
	    }
	}
	
	// Add content to these methods:
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}
	
	public function getInputFilter()
	{
	    if (!$this->inputFilter) {
	        $inputFilter = new InputFilter();
	        

	        $inputFilter->add(array(
	            'name'     => 'descricao',
	            'required' => true,
	            'filters'  => array(
	                array('name' => 'StripTags'),
	                array('name' => 'StringTrim'),
	            ),
	            'validators' => array(
	                /* array(
	                 'name' =>'NotEmpty',
	                 'options' => array(
	                 'messages' => array(
	                 \Zend\Validator\NotEmpty::IS_EMPTY => 'Entre com a descrição!'
	                 ),
	                 ),
	                 ),*/
	                array(
	                    'name'    => 'StringLength',
	                    'options' => array(
	                        'encoding' => 'UTF-8',
	                        'min'      => 1,
	                        'max'      => 50,
	                        'messages' => array(
	                            'stringLengthTooShort' => 'A descrição de conter de 1 a 50 characteres!',
	                            'stringLengthTooLong' => 'A descrição de conter de 1 a 50 characteres!'
	                        ),
	                    ),
	                ),
	            ),
	        ));
	        
	        
	        
	        $this->inputFilter = $inputFilter;
	    }
	    
	    return $this->inputFilter;
	}

}