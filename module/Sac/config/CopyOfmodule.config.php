<?php
namespace Sac;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
//use Zend\ServiceManager\Factory\InvokableFactory;


return array(
    'controllers' => array(
        'factories' => [
            //Controller\ChamadosController::class => InvokableFactory::class,
        ],
        
    ),
    
    'router' => [
        'routes' => [          
            'index' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/index',
                    'defaults' => [
                        'controller' => Controller\ChamadosController::class,
                        'action'     => 'index',
                        'div'        => 'aba_chamado',
                    ],
                ],
            ],
            
            'sac' => [
                'type'    => Segment::class,
                'options' => [
                    //'route'    => '/users[/:action[/:id]]',
                    'route' => "/sac/chamados[[/:action][/id/:id][/page/:page][/:div][/order_by/:order_by][/:order][/search_frase/:search_frase][/data_ini/:data_ini][/data_fin/:data_fin][/tipo_view/:tipo_view]]",
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'page'   => '[0-9]+',
                                'id'     => '[0-9]+',
                                'order_by' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'order' => 'ASC|DESC',
                    ],
                    'defaults' => [
                        'controller'    => Controller\ChamadosController::class,
                                'action'        => 'index',
                                'order' => 'ASC',
                                'page' => 1,
                                'search_frase' => '',
                    ],
                ],
            ],
        ],
    ],

	'service_manager' => [
		'factories' => [
			//ChamadosListener::class => ListenerFactory::class,
		],
	],
   
    'view_manager' => array(

        'template_path_stack' => array(
            'sac' => __DIR__ . '/../view',            
        ),
        'strategies' => array(
    		'ViewJsonStrategy',
        ),
    ),
   
	'doctrine' => [
		'driver' => [
			__NAMESPACE__ . '_driver' => [
				'class' => AnnotationDriver::class,
				'cache' => 'array',
				'paths' => [__DIR__ . '/../src/Entity']
			],
			'orm_default' => [
				'drivers' => [
					__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
				]
			]
		]
	]
);