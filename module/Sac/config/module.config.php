<?php
namespace Sac;

use Zend\Router\Http\Segment;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
//use Zend\ServiceManager\Factory\InvokableFactory;


return array(
    'controllers' => array(
        'factories' => [
            //Controller\ChamadosController::class => InvokableFactory::class,
        ],
        
    ),
    
    'router' => [
        'routes' => [          
            'chamados' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => "/chamados[/:id]",
                    'constraints' => [
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller'    => Controller\ChamadosController::class,
                                'order' => 'ASC',
                                'page' => 1,
                                'search_frase' => '',
                    ],
                ],
            ],
            
            'nextChamado' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => "/nextChamado[[/:action][/:id]]",
                    'constraints' => [
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller'    => Controller\ChamadosController::class,
                        'order' => 'ASC',
                        'page' => 1,
                        'search_frase' => '',
                    ],
                ],
            ],
            
            'prevChamado' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => "/prevChamado[[/:action][/:id]]",
                    'constraints' => [
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller'    => Controller\ChamadosController::class,
                        'order' => 'ASC',
                        'page' => 1,
                        'search_frase' => '',
                    ],
                ],
            ],
            
            'clientes' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => "/clientes[/:id]",
                    'constraints' => [
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller'    => Controller\ClientesController::class,
                        'order' => 'ASC',
                        'page' => 1,
                        'search_frase' => '',
                    ],
                ],
            ],
            
            'pedidos' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => "/pedidos[/:id]",
                    'constraints' => [
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller'    => Controller\PedidosController::class,
                        'order' => 'ASC',
                        'page' => 1,
                        'search_frase' => '',
                    ],
                ],
            ],
        ],
    ],

	'service_manager' => [
		'factories' => [
			//ChamadosListener::class => ListenerFactory::class,
		],
	],
   
    'view_manager' => array(

        'template_path_stack' => array(
            'sac' => __DIR__ . '/../view',            
        ),
        'strategies' => array(
    		'ViewJsonStrategy',
        ),
    ),
   
	'doctrine' => [
		'driver' => [
			__NAMESPACE__ . '_driver' => [
				'class' => AnnotationDriver::class,
				'cache' => 'array',
				'paths' => [__DIR__ . '/../src/Entity']
			],
			'orm_default' => [
				'drivers' => [
					__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
				]
			]
		]
	]
);