<?php

namespace Sac\Controller;


use Controle\Controller\AbstractRestController;
use Sac\Entity\Chamados;
use Zend\View\Model\JsonModel;


class ChamadosController extends AbstractRestController
{
    private function getVariaveis()
    {
        //$this->form = $this->container->get('FormElementManager')->get('Sac\Form\ChamadosForm');
        $this->route = 'sac';
        $this->viewData = 'dados';
        $this->pagination = true;
        $this->template = 'sac/chamados/index.phtml';
        $this->div = '';
        $this->primaryKey = null;
        $this->order_by = 'id';
        $this->entity = '\Sac\Entity\Chamados';
        $this->model = new Chamados();
        $this->searchFrase = '';
        $this->searchDate = '';
        $this->foreignKeys = array();
    }
    
    
    public function getList()
    {   
        $this->getVariaveis();
        
        return parent::getList();
    }
    
    
    public function get($id)
    {
        $this->getVariaveis();
        
        //return new JsonModel(array("data" => $id));
        
        return parent::get($id);
    }
    
    public function create($data) 
    {
        $this->getVariaveis();
        
        $this->getForeignKeys($data);
        
        return parent::create($data);

    }
    
    public function update($id, $data) 
    {
        $this->getVariaveis();        
        
        $this->getForeignKeys($data);

        return  parent::update($id, $data);
    }
    
    public function delete($id) 
    {
        $this->getVariaveis();
        
       return parent::delete($id);
    }
    
    public function prevAction()
    {
        $this->getVariaveis();
        
        return parent::prevAction();
    }
    
    public function nextAction()
    {
        $this->getVariaveis();        
        
        return parent::nextAction();
    }
    
    private function getForeignKeys($data) 
    {
        $cliente = $data['cliente_id'];
        $pedido = $data['pedido_id'];
        
        if (is_numeric($cliente) || is_numeric($pedido)) {
            array_push($this->foreignKeys, array("Sac\Entity\Clientes", 'setClienteId',  $cliente));
            array_push($this->foreignKeys, array("Sac\Entity\Pedidos", 'setPedidoId', $pedido));
        }  
    }
}
