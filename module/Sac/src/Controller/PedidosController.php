<?php

namespace Sac\Controller;


use Controle\Controller\AbstractRestController;
use Zend\View\Model\JsonModel;

class PedidosController extends AbstractRestController
{
    private function getVariaveis()
    {
        $this->route = 'sac';
        $this->viewData = 'dados';
        $this->pagination = true;
        $this->template = 'sac/pedidos/index.phtml';
        $this->div = '';
        $this->primaryKey = null;
        $this->order_by = 'id';
        $this->entity = '\Sac\Entity\Pedidos';
        $this->searchFrase = '';
        $this->searchDate = '';
    }
    
    
    public function getList()
    {   
        $this->getVariaveis();
        
        $dados = array(
            "id" => 1,
            "descricao" => 'clientes'
        );
        
        //return new JsonModel(array("resultSet" => $dados));
        
        return parent::getList();
    }
    
    
    public function get($id)
    {
        $this->getVariaveis();
        
        return new JsonModel(array("data" => $id));
    }
    
 
}
