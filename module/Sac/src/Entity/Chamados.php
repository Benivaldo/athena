<?php
namespace Sac\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Sac\Repository\ChamadosRepository")
 * @ORM\Table(name="chamados")
 */
class Chamados
{
	protected $inputFilter;
	
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(name="id")
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="titulo")
	 */
	protected $titulo;
	
	/**
	 * @ORM\Column(name="observacao")
	*/
	protected $observacao;

	/**
	 * @ORM\Column(name="email")
	*/
	protected $email;
		
	/**
    * @var \Sac\Entity\Chamados
	*
	* @ORM\ManyToOne(targetEntity="\Sac\Entity\Clientes", inversedBy="chamados")
	* @ORM\JoinColumns({
	*   @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
	* })
	*/
	protected $clienteId;
	
	/**
	 * @var datetime
	 * @ORM\Column(name="data_cadastro")
	 */
	protected $dataCadastro;
	
	/**
	 * @var datetime
	 * @ORM\Column(name="data_altera")
	 */	
	protected $dataAltera;
	
	/**
	 * @var \Sac\Entity\Chamados
	 *
	 * @ORM\ManyToOne(targetEntity="\Sac\Entity\Pedidos", inversedBy="chamados")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
	 * })
	 */
	protected $pedidoId;
	
	/*public function __construct()
	{
		$this->clientes = new ArrayCollection();
		$this->pedidos = new ArrayCollection();
	}*/
	
	/**
	 * Returns ID of this chamados
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Sets ID of this chamados.
	 * @param integer $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	
	/**
	 * Returns titulo.
	 */
	public function getTitulo()
	{
		return $this->titulo;
	}
	
/**
	 * Sets titulo.
	 * @param string $titulo
	 * @return \Sac\Entity\Chamados
	 */
	public function setTitulo($titulo)
	{
		$this->titulo = $titulo;
	
	}
	
	/**
	 *  Returns clientes.
	 */
	public function getClienteId()
	{
		return $this->clienteId;
	}
	
	/**
     * Set clientes
     * @param \Sac\Entity\Clientes $clienteId
     * @return Chamados
    */
	public function setClienteId(\Sac\Entity\Clientes $clienteId = null)
	{
		$this->clienteId = $clienteId;
		return $this;
	}

	/**
	 * Returns pedidos.
	 * 
	 */
	public function getPedidoId()
	{
		return $this->pedidoId;
	}
	
	/**
     * Set result
     *
     * @param \Sac\Entity\Pedidos $pedidoId
     * @return Chamados
     */
	public function setPedidoId(\Sac\Entity\Pedidos $pedidoId = null)
	{
		$this->pedidoId = $pedidoId;
		return $this;
	}
	
	/**
	 * Returns email.
	 */
	public function getEmail()
	{
		return $this->email;
	}
	
	/**
	 * Sets email.
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}
	
	/**
	 * Returns observacao.
	 */
	public function getObservacao()
	{
	    return $this->observacao;
	}
	
	/**
	 * Sets observacao.
	 * @param string $observacao
	 */
	public function setObservacao($observacao)
	{
	    $this->observacao = $observacao;
	}
	
	/**
	 * Gets triggered only on insert
	 * @ORM\PrePersist
	 */
	public function onPrePersist()
	{
		$this->dataCadastro = date('Y-m-d');
		$this->dataAltera = date('Y-m-d');
	}
	
	/**
	 * Gets triggered every time on update
	 * @ORM\PreUpdate
	 */
	public function onPreUpdate()
	{
		$this->dataAltera = date('Y-m-d');
	}
	
	public function exchangeArray($data)
	{
	    foreach ($data as $key => $value) {
	        $this->$key = (!empty($value) ? $value: null);
	    }
	}
	
	// Add content to these methods:
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}
	
	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
	
			$inputFilter->add(array(
					'name'     => 'titulo',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 80,
											'messages' => array(
													'stringLengthTooShort' => 'A descrição deve conter de 1 a 100 characteres!',
													'stringLengthTooLong' => 'A descrição deve conter de 1 a 100 characteres!'
											),
									),
							),
					),
			));
			
			$inputFilter->add(array(
					'name'     => 'email',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 80,
											'messages' => array(
													'stringLengthTooShort' => 'O e-mail deve conter de 1 a 100 characteres!',
													'stringLengthTooLong' => 'O e-mail deve conter de 1 a 100 characteres!'
											),
									),
							),
					),
			));
	
			$this->inputFilter = $inputFilter;
		}
	
		return $this->inputFilter;
	}

}