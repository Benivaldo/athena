<?php
namespace Sac\Repository;

use Doctrine\ORM\EntityRepository;
use Sac\Entity\Chamados;
use Zend\Db\Sql\Where;

class ChamadosRepository extends EntityRepository
{
    /**
     * Busca por id
     * @param integer $id
     * @return array
     */
    public function findById($id)
    {
        $entityManager = $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();
    
        $queryBuilder
        ->select()
        ->addSelect(
            't.id, t.titulo, t.email, t.observacao'
            )
        ->addSelect(
            'c.id as cliente_id'
            )
        ->addSelect(
            'p.id as pedido_id'
            )
        ->from(Chamados::class, 't')
        ->innerJoin('t.clienteId', 'c')
        ->innerJoin('t.pedidoId', 'p')   
        ->where('t.id = ?1')
        ->setParameter(1, $id);
       
        $chamados = $queryBuilder->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
 
        return $chamados;
    }
    
    /**
     * 
     * @param string $orderBy
     * @param string $order
     * @param string $search
     * @param string $data_ini
     * @param string $data_fin
     * @return \Doctrine\ORM\Query
     */
    public function findAllData($orderBy, $order, $search = '', $data_ini = null, $data_fin = null)
    {
        $entityManager =  $this->getEntityManager();       
            
        $queryBuilder = $entityManager->createQueryBuilder();
        //Select normal 
        $queryBuilder
            ->select('t', 'c', 'p')
            ->from(Chamados::class, 't')
            ->innerJoin('t.clienteId', 'c')
            ->innerJoin('t.pedidoId', 'p');    	
        
    	//Campo where   
        $queryBuilder->where($queryBuilder->expr()->orX(
        		$queryBuilder->expr()->eq('t.id','?1'),
        		$queryBuilder->expr()->eq('t.pedidoId','?1'),
    	       	$queryBuilder->expr()->like('t.email', '?2')
       	));
        
        $params = array('1' => (int)$search, '2' => '%'.$search.'%');
        
        //Campo de pesquisa data
       	if (!empty($data_ini) && !empty($data_fin)) {
       		$queryBuilder->andWhere($queryBuilder->expr()->andX( 
       				$queryBuilder->expr()->between(
    	  				't.dataCadastro',
    	      			':from',
    	      			':to'
       				)
    	        )
    	 	);
       		
       		$params['from'] = $data_ini;
       		$params[ 'to'] = $data_fin;
      	}
      	
      	//Campos as serem ordenados
      	switch ($orderBy) {
      		default:
      			$campo = "t.$orderBy";
      			break;
      		case 'nome':
      			$campo = "c.nome";
      			break;
      	}
            
        $queryBuilder->orderBy($campo, $order);
      	
      	$queryBuilder->setParameters($params);
        
        $chamados = $queryBuilder->getQuery();
    
        return $chamados;
    } 
  
}