<?php
namespace Sac\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Zend\Db\Sql\Where;
use Sac\Entity\Clientes;


class ClientesRepository extends EntityRepository
{
    // Finds all published posts having the given tag.
    public function findClientesByEmail($email)
    {
        $entityManager = $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();
     
        $queryBuilder->select('p')
            ->from(Clientes::class, 'p')
            ->where('p.email = ?1')
    	   ->setParameter(1, $email);

        $clientes = $queryBuilder->getQuery()->getResult();
        return $clientes;
    }
    
    /**
     *
     * @param string $orderBy
     * @param string $order
     * @param string $search
     * @param string $data_ini
     * @param string $data_fin
     * @return \Doctrine\ORM\Query
     */
    public function findAllData($orderBy, $order, $search = '', $data_ini = null, $data_fin = null)
    {
        $entityManager =  $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();
        
        //Select normal
        $queryBuilder->select('c')->from(Clientes::class, 'c');
                
        $clientes = $queryBuilder->getQuery();
        return $clientes;
    } 
}