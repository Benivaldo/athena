<?php
namespace Sac\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Zend\Db\Sql\Where;
use Sac\Entity\Pedidos;


class PedidosRepository extends EntityRepository
{
    /**
     * Busca por id
     * @param integer $id
     * @return array
     */
    public function findById($id)
    {
        $entityManager = $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();
     
        $queryBuilder->select('p')
            ->from(Pedidos::class, 'p')
            ->where('p.id = ?1')
    	   ->setParameter(1, $id);

        $pedidos = $queryBuilder->getQuery()->getResult();
        return $pedidos;
    }
    
    /**
     *
     * @param string $orderBy
     * @param string $order
     * @param string $search
     * @param string $data_ini
     * @param string $data_fin
     * @return \Doctrine\ORM\Query
     */
    public function findAllData($orderBy, $order, $search = '', $data_ini = null, $data_fin = null)
    {
        $entityManager =  $this->getEntityManager();
        
        $queryBuilder = $entityManager->createQueryBuilder();
        
        //Select normal
        $queryBuilder->select('p')->from(Pedidos::class, 'p');
                
        $pedidos = $queryBuilder->getQuery();
        return $pedidos;
    } 
}